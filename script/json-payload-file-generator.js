const fs = require('fs');
const path = require('path');
const readline = require('readline');

function readConfigFileandgenerateTS() {
  try {
    const configFile = fs.readdirSync('./config/');

    configFile.forEach((file) => {
      const filePath = path.join('./config/', file);
      const fileStream = fs.createReadStream(filePath);
      const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity, // Recognize all instances of CR LF ('\r\n') as a single line break
      });

      rl.on('line', (line) => {
        generateJsonFromDirectory('.'+line.split('-')[1], line.split('-')[0]);
      });
      rl.on('close', () => {
        console.log('File reading completed.');
      });
    });

    console.table(configFile);
  } catch (error) {
    console.error("File reading error:", error.message);
  }
}

function generateJsonFromDirectory(directoryPath, filename) {
  let outputPath = './src/app/constant/auto/' + filename + '.constant.ts';
  try {
    const files = fs.readdirSync(directoryPath);
    const jsonResult = [];
    files.forEach(file => {
      const filePath = path.join(directoryPath, file);
      const stats = fs.statSync(filePath);
      if (stats.isFile()) {
        let jsonPath = { title: '', filename: '', link: '' };
        // remove extension from file name, filename may contain multiple dots
        jsonPath.title = file.split('.').slice(0, -1).join('.');
        jsonPath.link = directoryPath.replace('/src/','/').replace('src/','./') + file;
        jsonPath.filename = directoryPath.replace('/src/','/').replace('src/','./') + file;
        jsonResult.push(jsonPath);
      } else if (stats.isDirectory()) {
        // If it's a directory, recursively read its contents
        console.log('Directory:', filePath.replaceAll('\\','/'));
        const subDirectoryResult = generateJsonFromDirectory(filePath.replaceAll('\\','/'), filename);
        jsonResult.push(...subDirectoryResult);
      }
    });
    fs.writeFileSync(
      outputPath,
      'export const ' + filename + '=' + JSON.stringify(jsonResult,null, 2)
    );
    console.log(`JSON file generated at ${outputPath}`);
    return jsonResult;
  } catch (error) {
    console.error('Error generating JSON file:', error);
    return [];
  }
}

const outputPath = '';

readConfigFileandgenerateTS();