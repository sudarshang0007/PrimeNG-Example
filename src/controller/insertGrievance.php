<?php
// Establishing the database connection
$servername = "localhost";
//$servername = "srv179.hstgr.io:3306";
$username = "u421383828_sud";
$password = "P@ssw0rd";
$dbname = "u421383828_sud";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Handling GET requests
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    // Get all grievances
    if (empty($_GET['id'])) {
        $sql = "SELECT * FROM grievances";
        $result = $conn->query($sql);

        $grievances = array();
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $grievances[] = $row;
            }
        }

        echo json_encode($grievances);
    }
    // Get a specific grievance by ID
    else {
        $id = $_GET['id'];
        $sql = "SELECT * FROM grievances WHERE id = $id";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $grievance = $result->fetch_assoc();
            echo json_encode($grievance);
        } else {
            echo "Grievance not found.";
        }
    }
}

// Handling POST requests
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    $title = $_POST['title'];
    $name = $_POST['name'];
    $contact = $_POST['contact'];
    $email= $_POST['email'];
    $type = $_POST['type'];
    $details = $_POST['details'];
    $date = date('m/d/Y h:i:s a', time());
    //$date = date('Y-m-d H:i:s', strtotime($_POST['date']));
    $reporterType = $_POST['reporterType'];

    $sql = "INSERT INTO grievances (title, name, contact,email, type, details, date, reporterType, status)
            VALUES ('$title', '$name', '$contact', '$email', '$type', '$details', '$date', '$reporterType', 0)";

    if ($conn->query($sql) === true) {
        $response = array(
            'status' => 'success',
            'message' => 'Grievance details saved successfully.'
        );
        echo json_encode($response);
    } else {
        $response = array(
            'status' => 'error',
            'message' => 'Error: ' . $sql . "<br>" . $conn->error
        );
        echo json_encode($response);
    }
}

// Handling PUT requests
if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    parse_str(file_get_contents("php://input"), $putVars);

    $id = $putVars['id'];
    $title = $putVars['title'];
    $name = $putVars['name'];
    $contact = $putVars['contact'];
    $type = $putVars['type'];
    $details = $putVars['details'];
    $date = date('Y-m-d H:i:s', strtotime($putVars['date']));
    $reporterType = $putVars['reporterType'];

    $sql = "UPDATE grievances SET 
            title = '$title',
            name = '$name',
            contact = '$contact',
            type = '$type',
            details = '$details',
            date = '$date',
            reporterType = '$reporterType'
            WHERE id = $id";

    if ($conn->query($sql) === true) {
        $response = array(
            'status' => 'success',
            'message' => 'Grievance details updated successfully.'
        );
        echo json_encode($response);
    } else {
        $response = array(
            'status' => 'error',
            'message' => 'Error: ' . $sql . "<br>" . $conn->error
        );
        echo json_encode($response);
    }
}

// Handling DELETE requests
if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    
    $id = $_SERVER['HTTP_CLIENT'];
    
    parse_str(file_get_contents("php://input"), $deleteVars);

   //  $id = $_DELETE['id'];
   // $id = $deleteVars['id'];

    $sql = "DELETE FROM grievances WHERE grievances.id = $id";

    if ($conn->query($sql) === true) {
        $response = array(
            'status' => 'success',
            'message' => 'Grievance deleted successfully.' . 'id: '. $id
        );
        echo json_encode($response);
    } else {
        $response = array(
            'status' => 'error',
            'message' => 'Error: ' . $sql . 'Variable: '. $id .  "<br>" . $conn->error
        );
        echo json_encode($response);
    }
}

// Closing the database connection
$conn->close();
?>
