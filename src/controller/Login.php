<?php
// Establishing the database connection
$servername = "localhost";
//$servername = "srv179.hstgr.io";
$username = "u421383828_sud";
$password = "P@ssw0rd";
$dbname = "u421383828_sud";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Handling GET requests
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    // Handle GET request logic here
    // Example: Retrieve data from the database and return as JSON
    $response = array(
        'status' => 'success',
        'message' => 'GET request handled successfully.'
    );
    echo json_encode($response);
}


// Handling POST requests
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Validate user credentials
    $sql = "SELECT * FROM users WHERE username = '$username' AND password = '$password'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // User credentials are valid
        $response = array(
            'status' => 'success',
            'message' => 'Login successful.'
        );
        echo json_encode($response);
    } else {
        // User credentials are invalid
        $response = array(
            'status' => 'error',
            'message' => 'Invalid username or password.'
        );
        echo json_encode($response);
    }
}

// Handling PUT requests
if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    // Handle PUT request logic here
    // Example: Update data in the database
    $response = array(
        'status' => 'success',
        'message' => 'PUT request handled successfully.'
    );
    echo json_encode($response);
}

// Handling DELETE requests
if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
    // Handle DELETE request logic here
    // Example: Delete data from the database
    $response = array(
        'status' => 'success',
        'message' => 'DELETE request handled successfully.'
    );
    echo json_encode($response);
}

// Closing the database connection
$conn->close();
?>
