import { TestBed } from '@angular/core/testing';

import { URLSanitizerService } from './urlsanitizer.service';

describe('URLSanitizerService', () => {
  let service: URLSanitizerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(URLSanitizerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
