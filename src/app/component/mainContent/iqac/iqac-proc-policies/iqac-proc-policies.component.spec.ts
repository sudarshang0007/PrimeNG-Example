import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqacProcPoliciesComponent } from './iqac-proc-policies.component';

describe('IqacProcPoliciesComponent', () => {
  let component: IqacProcPoliciesComponent;
  let fixture: ComponentFixture<IqacProcPoliciesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqacProcPoliciesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqacProcPoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
