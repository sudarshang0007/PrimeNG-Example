import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqacCommitteeComponent } from './iqac-committee.component';

describe('IqacCommitteeComponent', () => {
  let component: IqacCommitteeComponent;
  let fixture: ComponentFixture<IqacCommitteeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqacCommitteeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqacCommitteeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
