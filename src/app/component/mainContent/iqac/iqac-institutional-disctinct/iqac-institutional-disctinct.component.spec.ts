import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqacInstitutionalDisctinctComponent } from './iqac-institutional-disctinct.component';

describe('IqacInstitutionalDisctinctComponent', () => {
  let component: IqacInstitutionalDisctinctComponent;
  let fixture: ComponentFixture<IqacInstitutionalDisctinctComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqacInstitutionalDisctinctComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqacInstitutionalDisctinctComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
