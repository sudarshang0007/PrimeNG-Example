import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqacMomComponent } from './iqac-mom.component';

describe('IqacMomComponent', () => {
  let component: IqacMomComponent;
  let fixture: ComponentFixture<IqacMomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqacMomComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqacMomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
