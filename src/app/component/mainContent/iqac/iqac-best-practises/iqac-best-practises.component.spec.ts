import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqacBestPractisesComponent } from './iqac-best-practises.component';

describe('IqacBestPractisesComponent', () => {
  let component: IqacBestPractisesComponent;
  let fixture: ComponentFixture<IqacBestPractisesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqacBestPractisesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqacBestPractisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
