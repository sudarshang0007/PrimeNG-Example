import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqacActionTakenComponent } from './iqac-action-taken.component';

describe('IqacActionTakenComponent', () => {
  let component: IqacActionTakenComponent;
  let fixture: ComponentFixture<IqacActionTakenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqacActionTakenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqacActionTakenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
