import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqacAqarDocsComponent } from './iqac-aqar-docs.component';

describe('IqacAqarDocsComponent', () => {
  let component: IqacAqarDocsComponent;
  let fixture: ComponentFixture<IqacAqarDocsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqacAqarDocsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqacAqarDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
