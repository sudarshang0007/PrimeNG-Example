import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqcsCompositionComponent } from './iqcs-composition.component';

describe('IqcsCompositionComponent', () => {
  let component: IqcsCompositionComponent;
  let fixture: ComponentFixture<IqcsCompositionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqcsCompositionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqcsCompositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
