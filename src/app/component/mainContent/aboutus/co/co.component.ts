import { Component, OnInit } from '@angular/core';
import { URLSanitizerService } from 'src/app/component/service/urlsanitizer.service';

@Component({
  selector: 'app-co',
  templateUrl: './co.component.html',
  styleUrls: ['./co.component.css']
})
export class CoComponent implements OnInit {

  constructor(public santizerService: URLSanitizerService) { }

  ngOnInit(): void {
  }

}
