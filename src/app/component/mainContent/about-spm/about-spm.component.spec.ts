import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutSpmComponent } from './about-spm.component';

describe('AboutSpmComponent', () => {
  let component: AboutSpmComponent;
  let fixture: ComponentFixture<AboutSpmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutSpmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AboutSpmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
