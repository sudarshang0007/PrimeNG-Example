import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DvvComponent } from './dvv.component';

describe('DvvComponent', () => {
  let component: DvvComponent;
  let fixture: ComponentFixture<DvvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DvvComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DvvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
