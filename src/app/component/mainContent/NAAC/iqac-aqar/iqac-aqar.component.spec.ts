import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IqacAqarComponent } from './iqac-aqar.component';

describe('IqacAqarComponent', () => {
  let component: IqacAqarComponent;
  let fixture: ComponentFixture<IqacAqarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IqacAqarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IqacAqarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
