import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NaacAddionalLinkComponent } from './naac-addional-link.component';

describe('NaacAddionalLinkComponent', () => {
  let component: NaacAddionalLinkComponent;
  let fixture: ComponentFixture<NaacAddionalLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NaacAddionalLinkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NaacAddionalLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
