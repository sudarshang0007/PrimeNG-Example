import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionalDistinctivenessComponent } from './institutional-distinctiveness.component';

describe('InstitutionalDistinctivenessComponent', () => {
  let component: InstitutionalDistinctivenessComponent;
  let fixture: ComponentFixture<InstitutionalDistinctivenessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstitutionalDistinctivenessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InstitutionalDistinctivenessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
