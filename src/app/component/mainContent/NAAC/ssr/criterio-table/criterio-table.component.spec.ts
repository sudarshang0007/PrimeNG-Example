import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriterioTableComponent } from './criterio-table.component';

describe('CriterioTableComponent', () => {
  let component: CriterioTableComponent;
  let fixture: ComponentFixture<CriterioTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriterioTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CriterioTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
