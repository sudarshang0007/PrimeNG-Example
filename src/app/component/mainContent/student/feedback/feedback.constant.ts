export const feedbackList = [
  { title: "2017-18", link: "./assets/documentation/student/feedback/2017-18 action taken report.pdf" },
  { title: "2018-19", link: "./assets/documentation/student/feedback/2018-19 action taken report.pdf" },
  { title: "2019-20", link: "./assets/documentation/student/feedback/2019-20 action taken report.pdf" },
  { title: "2020-21", link: "./assets/documentation/student/feedback/2020-21 action taken report.pdf" },
  { title: "2021-22", link: "./assets/documentation/student/feedback/2021-22 action taken report.pdf" },
];
