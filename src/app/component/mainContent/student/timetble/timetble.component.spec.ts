import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimetbleComponent } from './timetble.component';

describe('TimetbleComponent', () => {
  let component: TimetbleComponent;
  let fixture: ComponentFixture<TimetbleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimetbleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TimetbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
