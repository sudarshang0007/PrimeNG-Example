import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatTreeModule } from "@angular/material/tree";

@NgModule({
  declarations: [],
  imports: [CommonModule, MatTreeModule],
  exports: [CommonModule, MatTreeModule],
})
export class MaterialModule {}
