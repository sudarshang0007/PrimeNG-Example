export const dvv=[
  {
    "title": "1.2.1",
    "filename": "./assets/documentation/naac/dvv/11.2.1.pdf",
    "link": "./assets/documentation/naac/dvv/11.2.1.pdf"
  },
  {
    "title": "1.2.2",
    "filename": "./assets/documentation/naac/dvv/11.2.2.pdf",
    "link": "./assets/documentation/naac/dvv/11.2.2.pdf"
  },
  {
    "title": "1.3.2",
    "filename": "./assets/documentation/naac/dvv/11.3.2.pdf",
    "link": "./assets/documentation/naac/dvv/11.3.2.pdf"
  },
  {
    "title": "1.4.1",
    "filename": "./assets/documentation/naac/dvv/11.4.1.pdf",
    "link": "./assets/documentation/naac/dvv/11.4.1.pdf"
  },
  {
    "title": "1.1",
    "filename": "./assets/documentation/naac/dvv/1.1.pdf",
    "link": "./assets/documentation/naac/dvv/1.1.pdf"
  },
  {
    "title": "2.6.3",
    "filename": "./assets/documentation/naac/dvv/2.6.3.pdf",
    "link": "./assets/documentation/naac/dvv/2.6.3.pdf"
  },
  {
    "title": "4.1.2. FINAL UPLOAD",
    "filename": "./assets/documentation/naac/dvv/44.1.2. FINAL UPLOAD.pdf",
    "link": "./assets/documentation/naac/dvv/44.1.2. FINAL UPLOAD.pdf"
  },
  {
    "title": "4.3.2 FINAL UPLOADING",
    "filename": "./assets/documentation/naac/dvv/44.3.2 FINAL UPLOADING.pdf",
    "link": "./assets/documentation/naac/dvv/44.3.2 FINAL UPLOADING.pdf"
  },
  {
    "title": "4.4.1. FINAL UPLOAD",
    "filename": "./assets/documentation/naac/dvv/44.4.1. FINAL UPLOAD.pdf",
    "link": "./assets/documentation/naac/dvv/44.4.1. FINAL UPLOAD.pdf"
  },
  {
    "title": "Extended profile",
    "filename": "./assets/documentation/naac/dvv/4Extended profile.pdf",
    "link": "./assets/documentation/naac/dvv/4Extended profile.pdf"
  },
  {
    "title": "5.1.1",
    "filename": "./assets/documentation/naac/dvv/55.1.1.pdf",
    "link": "./assets/documentation/naac/dvv/55.1.1.pdf"
  },
  {
    "title": "5.1.2",
    "filename": "./assets/documentation/naac/dvv/55.1.2.pdf",
    "link": "./assets/documentation/naac/dvv/55.1.2.pdf"
  },
  {
    "title": "5.1.3",
    "filename": "./assets/documentation/naac/dvv/55.1.3.pdf",
    "link": "./assets/documentation/naac/dvv/55.1.3.pdf"
  },
  {
    "title": "5.1.4",
    "filename": "./assets/documentation/naac/dvv/55.1.4.pdf",
    "link": "./assets/documentation/naac/dvv/55.1.4.pdf"
  },
  {
    "title": "5.2.1",
    "filename": "./assets/documentation/naac/dvv/55.2.1.pdf",
    "link": "./assets/documentation/naac/dvv/55.2.1.pdf"
  },
  {
    "title": "5.2.2",
    "filename": "./assets/documentation/naac/dvv/55.2.2.pdf",
    "link": "./assets/documentation/naac/dvv/55.2.2.pdf"
  },
  {
    "title": "5.3.1",
    "filename": "./assets/documentation/naac/dvv/55.3.1.pdf",
    "link": "./assets/documentation/naac/dvv/55.3.1.pdf"
  },
  {
    "title": "5.3.2",
    "filename": "./assets/documentation/naac/dvv/55.3.2.pdf",
    "link": "./assets/documentation/naac/dvv/55.3.2.pdf"
  },
  {
    "title": "6.2.2.DVV",
    "filename": "./assets/documentation/naac/dvv/66.2.2.DVV.pdf",
    "link": "./assets/documentation/naac/dvv/66.2.2.DVV.pdf"
  },
  {
    "title": "6.3.2 DVV",
    "filename": "./assets/documentation/naac/dvv/66.3.2 DVV.pdf",
    "link": "./assets/documentation/naac/dvv/66.3.2 DVV.pdf"
  },
  {
    "title": "6.3.3 DVV",
    "filename": "./assets/documentation/naac/dvv/66.3.3 DVV.pdf",
    "link": "./assets/documentation/naac/dvv/66.3.3 DVV.pdf"
  },
  {
    "title": "6.5.2 DVV",
    "filename": "./assets/documentation/naac/dvv/66.5.2 DVV.pdf",
    "link": "./assets/documentation/naac/dvv/66.5.2 DVV.pdf"
  },
  {
    "title": "DVV 2.1.1",
    "filename": "./assets/documentation/naac/dvv/DVV 2.1.1.pdf",
    "link": "./assets/documentation/naac/dvv/DVV 2.1.1.pdf"
  },
  {
    "title": "DVV 2.1.2",
    "filename": "./assets/documentation/naac/dvv/DVV 2.1.2.pdf",
    "link": "./assets/documentation/naac/dvv/DVV 2.1.2.pdf"
  },
  {
    "title": "DVV 2.2.1",
    "filename": "./assets/documentation/naac/dvv/DVV 2.2.1.pdf",
    "link": "./assets/documentation/naac/dvv/DVV 2.2.1.pdf"
  },
  {
    "title": "DVV 2.4.2",
    "filename": "./assets/documentation/naac/dvv/DVV 2.4.2.pdf",
    "link": "./assets/documentation/naac/dvv/DVV 2.4.2.pdf"
  },
  {
    "title": "DVV 7.1.2",
    "filename": "./assets/documentation/naac/dvv/DVV 7.1.2.pdf",
    "link": "./assets/documentation/naac/dvv/DVV 7.1.2.pdf"
  },
  {
    "title": "DVV 7.1.3",
    "filename": "./assets/documentation/naac/dvv/DVV 7.1.3.pdf",
    "link": "./assets/documentation/naac/dvv/DVV 7.1.3.pdf"
  }
]