export const admissionLink = [
    { displayText: 'Admission Notification First Year B. Pharmacy 2024-2025', filename: '../assets/documentation/admission/Admission Notification First Year 2024-25.pdf', link: '' },
    { displayText: 'Admission Notification Direct Second Year 2024-2025', filename: '../assets/documentation/admission/Admission Notification Direct Second Year 2024-25.pdf', link: '' },
    { displayText: 'Admission Notification and Schedule', filename: '../assets/documentation/admission/21-22/Admission_Notification_and_Schedule22.pdf', link: '' },
    { displayText: 'Results', filename: '../assets/documentation/imp/20230128164050.pdf', link: '' },
];

