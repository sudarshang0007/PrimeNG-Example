export const NaacAddionalLink = [
  { title: "FDP programs attended", link: "./assets/documentation/naac/additional/FDP programs attended.pdf" },
  { title: "Policy dodument for financial support to faculty members", link: "./assets/documentation/naac/additional/Policy dodument for financial support to faculty members.pdf" },
];
